## -*- encoding: utf-8 -*-
import os
import sys
from setuptools import setup
from codecs import open # To open the README file with proper encoding
from setuptools.command.test import test as TestCommand # for tests

# Get information from separate files (README, VERSION)
def readfile(filename):
    with open(filename,  encoding='utf-8') as f:
        return f.read()
    
# For the tests
class SageTest(TestCommand):
    def run_tests(self):
        errno = os.system("sage -t --force-lib comb_walks")
        if errno != 0:
            sys.exit(1)

setup(
    name = "comb_walks",
    version = readfile("VERSION").strip(), # the VERSION file is shared with the documentation
    description='A Sage package for Walks in the Quarter Plane',
    # long_description = readfile("README.txt"), # get the long description from the README
    # For a Markdown README replace the above line by the following two lines:
    long_description = readfile("README.md"),
    long_description_content_type="text/markdown",
    url='https://gitlab.inria.fr/anjimene/da-equations-genus-1',
    author = "Antonio Jimenez-Pastor",
    author_email = "ajpastor@risc.uni-linz.ac.at",
    #license = "GPLv3+", # See LICENCE file
    classifiers=[
      # How mature is this project? Common values are
      #   3 - Alpha
      #   4 - Beta
      #   5 - Production/Stable
      'Development Status :: 3 - Alpa',
      'Intended Audience :: Science/Research',
      'Topic :: Software Development :: Build Tools',
      'Topic :: Scientific/Engineering :: Mathematics',
      #'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
      'Programming Language :: Python :: 2.7',
    ], # classifiers list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
    keywords = "walks SageMath elliptic",
    packages = ["comb_walks"],
    cmdclass = {'test': SageTest}, # adding a special setup command for tests
    setup_requires   = [],
    install_requires = ['deprecation','sphinx'],
)
    
