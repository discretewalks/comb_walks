.. nodoctest

Models of Walks in the Quarter Plane
====================================

.. automodule:: comb_walks.walkmodel
   :members:
   :undoc-members:
   :show-inheritance:
