.. nodoctest

Exceptions of package ``comb_walks``
=====================================

.. automodule:: comb_walks.exceptions
   :members:
   :undoc-members:
   :show-inheritance:
