.. nodoctest

Algebraic Geometry in Sage
==============================

.. automodule:: comb_walks.alggeo
   :members:
   :undoc-members:
   :show-inheritance:
